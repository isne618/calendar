<html>
<link rel="stylesheet" type="text/css" href="design.css">
<head>
	<title>Calendar</title>
   <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js" type="text/javascript"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<script >
        $(function() {
          $('.normal_date').click(function() {
            $('#contactForm').fadeToggle();
            var day = $(this).find(".dayIndi").text();
            if(day.length == 1)
            {
              day = "0"+day;
            }
            $('#date').val($("#yearIndi").attr('value') + "-" + $("#monthIndi").attr('value') + "-" + day);
            console.log(day + "-" + $("#monthIndi").attr('value') + "-" + $("#yearIndi").attr('value') );
          });

          $('#contact').click(function() {
            $('#contactForm').fadeToggle(200);
            $('#date').val("");
          });

          $(document).mouseup(function (e) {
            var container = $("#contactForm");

            if (!container.is(e.target) 
                && container.has(e.target).length === 0) 
            {
                container.fadeOut();
            }
          });
      });

</script>

<script>
        $(document).ready(function(){

                $(".draggable").draggable({
                  revert:"invalid",
                  snap: true,
                  snapMode: "inner"
                });

                $(".droppable").droppable({
                  accept: ".draggable",
                  drop: function(event, ui)
                  {
                    var dropped = ui.draggable;
                          var id = dropped.attr('id');

                          var date_i = $(this).find('span.dayIndi').text();

                    date_temp = year_month + '-' + date_i;
                    console.log(date_temp + " THIS ID: " + id);


                    var post_data = {
                      event_id: id,
                      date: date_temp
                    };
                    $.post("handler.php", post_data, function(data, status) 
                    {
                      location.reload();
                    });
                  }
                });
                console.log(year_month);
              });
      </script>


<body style="background-color: #FEFFF1" class="body">
<?php
include_once 'excode/includes/db_connect.php';
include_once 'excode/includes/functions.php';

sec_session_start();
if (!(login_check($mysqli) == true))
{
   header('Location: excode/index.php');
   exit();
}
	if(isset($_GET['date'])){
    $date = $_GET['date'];

    $day = date('d', strtotime($date));      //Gets day of apx`pointment (1‐31) 
      $month = date('m', strtotime($date));      //Gets month of appointment (1‐12) 
      $year = date('Y', strtotime($date));      //Gets year of appointment (e.g. 2016) 
      $firstday = date('w', strtotime('01-' . $month . '-' . $year));  //Gets the day of the week for the 1st of  
                 //the month. (e.g. 0 for Sun, 1 for Mon) 
      $days = date('t', strtotime($date));      //Gets number of days in month 
    }else{
      $date = date('Y-m-d');

      $day = date('d', strtotime($date));      //Gets day of apx`pointment (1‐31) 
      $month = date('m', strtotime($date));      //Gets month of appointment (1‐12) 
      $year = date('Y', strtotime($date));      //Gets year of appointment (e.g. 2016) 
      $firstday = date('w', strtotime('01-' . $month . '-' . $year));  //Gets the day of the week for the 1st of  
                 //the month. (e.g. 0 for Sun, 1 for Mon) 
      $days = date('t', strtotime($date));      //Gets number of days in month 
    }

   if(isset($_GET['title'])){
    $title = $_GET['title']; //get appointment title
  }  

   $today = date('d');            //Gets today’s date 
   $todaymonth = date('m');          //Gets today’s month 
   $todayyear = date('Y');            //Gets today’s year 

   if(isset($_GET['detail'])){
    $detail = $_GET['detail'];
  }
  else{
    $detail = '';
  }

  if(isset($_GET['$start_time'])){
    $start_time = $_GET['start_time'];    
  }
   
  if(isset($_GET['$end_time'])){
    $end_time = $_GET['end_time'];  
  }
  if(isset($_GET['$color'])){
    $color = $_GET['color'];
  }
  date_default_timezone_set('Asia/Bangkok');
?>
  <script type="text/javascript">
  year_month = <?php echo '"' . date('Y-m',strtotime($date)) . '"' ?>

  </script>
<div style="position: relative; top: 10px;">
<?php
	$t_date = date("l, F d", strtotime("now"));
	echo '<a href="?date=' . date('Y-m-d',strtotime('-1 month', strtotime($date))) . '" title="Previous month"><button class="button">&#8810; Prev </button></a>';
	echo '<a href="?date=' . date('Y-m-d',strtotime('+1 month', strtotime($date))) . '" title="Next month"><button class="button2"> Next &#8811;</button></a>';
  echo '<a href="?date=' . date('Y-m-d', strtotime('now')) . '" title="'.$t_date.'"><button class="todaybutton"> Today </button></a>';

	$m1 = date('F', strtotime($date));
	echo '<div class="month"><span id="monthIndi" value="' . $month . '">' . $m1 . '</span> <span id="yearIndi" value="' . $year . '">' . $year . '</span></div>';

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "appointment";

if(isset($_GET['add_appo_btn']))
{
   $conn = new mysqli($servername,$username,$password,$dbname);
   $color = explode('#', $_GET['color']);
    $color = implode('', $color);
    if ($qry = "INSERT INTO `appo` (`id`, `username`, `title`, `date`, `start_time`, `end_time`, `detail`, `color`) 
    VALUES (NULL, '" .$_SESSION['user_id']. "', '".$title."', '".$_GET['date']."', '".$_GET['start_time']."', '".$_GET['end_time']."', '".$detail."', '".$color."')") {
      $color = explode('#', $_GET['color']);
      $color = implode('', $color);
      $conn->query($qry);
      echo $conn->error;
      $conn->close();

    header('Location: calendar.php?date='.$_GET['date']);
   }
}


if(isset($_GET['title'])){
    // $conn = new mysqli($servername,$username,$password,$dbname);
    //   if($conn->connect_error)   
    // {
    //     die("connection failed: " .$conn->connect_error);

    // }  
    //   $color = explode('#', $_GET['color']);
    //   $color = implode('', $color);
    // $qry = "INSERT INTO `appo` (`id`, `username`, `title`, `date`, `start_time`, `end_time`, `detail`, `color`) 
    // VALUES (NULL, '" .$_SESSION['user_id']. "', '".$title."', '".$_GET['date']."', '".$_GET['start_time']."', '".$_GET['end_time']."', '".$detail."', '".$color."')";  
    // //data of $_GET['...'] from add appo form 
    // $conn->query($qry);
    //   echo $conn->error;
    // $conn->close();
  }
?>

<!-- <script>
$(function(){
  /* เพิ่มฟังก์ชันที่จะเรียก Ajax เมื่อมีการคลิกลิงค์ที่อยู่ภายใต้ div ที่มี id="sidebar" */
  $('.dropdown-content').delegate('a', 'click', function(e){
    e.preventDefault();
    var link = this.href;
       
    /* ดึงเนื้อหาจากลิงค์ด้วย Ajax เมื่อผู้ใช้กดลิงค์ */
    $.get(link, function(res){
      /* อัพเดทเนื้อหาที่ได้จาก Ajax ไปที่ div ที่มี id="content" */
      $('.body').html(res);
      /* หลังจากอัพเดทเนื้อหาเสร็จ เปลี่ยน URL ของเบราว์เซอร์ */
      window.history.replaceState(null, null, link);
    });
  });
});
</script> -->

<div class="dropdown">
   <button class="dropbtn">&#9662;View</button>
   <div class="dropdown-content" id="d" >
      <a href="calendar_day.php">DAY</a>
      <a href="calendar_week.php">WEEK</a>
      <a href="calendar.php">MONTH</a>
      <a href="detail.php">SCHEDULE</a>
  </div>
</div>

<button class="logout"><a href="excode/includes/logout.php" style="text-decoration-line: none; color: white;">LOGOUT&#128682;</a></button>

<div id="contact">+CREATE</div>
   <div id="contactForm">
      <form method="get">
         <span id="title"><input type="text" class="form-control" name="title" id="title" placeholder="Add title" required></span><br>
         <span id="position_d"><label for="date">&#128198;</label>
         <input type="date" class="form-control" name="date" id="date" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 15px;"></span><br>
         <span id="position_t"><label>&#9201;</label>
         <label for="start_time">start </label>
         <input type="time" class="form-control" name="start_time" id="start_time" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 17px;"></span><br>
         <span id="position_t2">
         <label for="end_time">end </label>
         <input type="time" class="form-control" name="end_time" id="end_time" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 17px;"></span><br>
         <span id="position_des"><label for="detail">&#128203;</label>
         <input type="text" class="form-control" name="detail" id="detail" placeholder="Add description" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 15px;"></span><br>
         <span id="position_c">
         <label for="color" style="position: relative; top: -4px;">&#127912;</label>
         <input type="color" class="form-control" name="color" id="color" required></span><br>
         <input class="button3" name="add_appo_btn" type="submit" value="SAVE" />
         <input class="button4" type="reset" value="DISMISS" />
      </form>
   </div>

<hr style=" border: 2px solid black; background-color: black; width: 99%; position: absolute; top: 84px;">
<div class="vl"></div>
<div class="calendar">
	<div class="center">
   		<div class="days sun">Sunday</div> 
   		<div class="days mon">Monday</div> 
   		<div class="days tue">Tuesday</div> 
   		<div class="days wed">Wednesday</div> 
   		<div class="days thr">Thursday</div> 
   		<div class="days fri">Friday</div> 
   		<div class="days sat">Saturday</div>
	</div>


<?php 
   for($i=1; $i<=$firstday; $i++) 
   { 
      echo '<div class="date blankday"></div>'; 
   } 
?>
<?php
  for($i=1; $i<=$days; $i++) 
   { 
      echo '<div value="'.$i.'" class="droppable normal_date  date'; 
      if ($today == $i && $todaymonth==$month && $todayyear == $year) 
      { 
         echo ' today'; 
      } 
      echo '"><span class="dayIndi">' . $i . '</span><br>';

      	//select DB
         $id = $_SESSION['user_id'];
   		   $app_date = date($year.'-'.$month.'-'.$i);
         $mysqli = new mysqli("localhost", "root", "", "appointment"); //connent to DB
      	$sql = "SELECT * FROM appo WHERE date = '$app_date' AND `username` = '".$id."' ";
      	if($result = $mysqli->query($sql))
         {
            while($row = $result->fetch_assoc()){
               $start = date('H:i', strtotime($row['start_time']));
               $end = date('H:i', strtotime($row['end_time']));
               $details = $row['detail'];
               $colors = $row['color'];

               if(date('d',strtotime($row['date']))== $i )
               {
                 

                  echo '<span class="draggable" id="'.$row['id'].'"><div title = "start: '.$start.'&#xA;end: '.$end.'&#xA;description: '.$details.' "><span style="background-color: '.$colors.';">'.$row['title'].'</span></div></span>'; 
                 
                   


               }

            }


         }

         //echo 'Start ' . $start_time . ' End ' . $end_time;

         $mysqli->close();

      echo  '</div>';
   } 
?> 
<?php 
   $daysleft = 7-(($days + $firstday)%7); 
   if($daysleft<7) 
   { 
      for($i=1; $i<=$daysleft; $i++) 
      { 
         echo '<div class=" date blankday"></div>'; 
      } 
   } 
?> 
</div>
</div>
</body>
</html>