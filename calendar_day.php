<?php
include_once 'excode/includes/db_connect.php';
  include_once 'excode/includes/functions.php';

sec_session_start();
if (!(login_check($mysqli) == true))
{
		header('Location: excode/index.php');
   		exit();
}

?>

<html>
		<head>
				<title>Daily Calendar</title>
				<meta charset="UTF-8">
				<link rel="stylesheet" href="main.css" />
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
				<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
				<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
				<link rel="shortcut icon" type="image/x-icon" href="image/cal3.ico">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
				<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
				<script src="https://kit.fontawesome.com/a076d05399.js"></script>
				<meta name="viewport" content="width=device-width, initial-scale=1">
		
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<script src="https://kit.fontawesome.com/a076d05399.js"></script>
				<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		</head>
<style>
.appointments
{
	text-align: center;
	font-family: monospace;
	font-size: 20px;
	max-width: 70px;
	margin-left: 2px;

}

</style>

<body style="background-color: #FEFFF1">

	<!-- <script>
		$(function(){
		  $('.dropdown-content').delegate('a', 'click', function(e){
		    e.preventDefault();
		    var link = this.href;
		       
		    $.get(link, function(res){
		      $('.box').html(res);
		      window.history.replaceState(null, null, link);
		    });
		  });
		});
  </script> -->

		


	<script>
		$(document).ready(function() {
			$(".draggable").draggable({
				revert: "invalid",
				snap: true,
				snapMode: "inner"
			});

			$(".droppable").droppable({
				accept: ".draggable",	
				drop: function(event, ui) {
					var drop_id = ui.draggable.attr('id');
					var drop = $(this).children(".hour").text();
					var day = $(this).children("#day_val").val();
               		var month = $(this).children("#month_val").val();
               		var year = $(this).children("#year_val").val();
               		var send_day = year + "-" + month + "-" + day;
					var send = drop;
					console.log(send);
					$.ajax({
						url:"handlerday.php",
						method:"POST",
                  		data:{start_time:send,date:send_day,event:drop_id},
						success:function(data) {
							if (!data) {
								alert('fail');
							}
						}
					});
				}
			});

		});
	</script>
	<script>$(function() { 
        $('.normal_date').click(function() {
          $('#contactForm').fadeToggle();
          var day = $(this).find("#day_val").val();
          if(day.length == 1)
          {
            day = "0"+day;
          }
          $('#date').val($("#year_val").attr('value') + "-" + $("#month_val").attr('value') + "-" + day);
          console.log(day + "-" + $("#month_val").attr('value') + "-" + $("#year_val").attr('value') );
          
          // var time = $(this).children(".hour").text();
          // var send_time = time + ":00:00";



        });

        $('#contact').click(function() {
          $('#contactForm').fadeToggle();
          $('#date').val("");
        });


        $(document).mouseup(function (e) {
          var container = $("#contactForm");

          if (!container.is(e.target) 
              && container.has(e.target).length === 0) 
          {
              container.fadeOut();
          }
        });

        
      });
    </script>

		
	<?php
	if(isset($_GET['date']))
	{
		$_SESSION['date'] = date("d-m-Y",strtotime($_GET['date']));
	}
	else if(!isset($_SESSION['date']))
	{
		$_SESSION['date'] = date("d-m-Y");
	}

	if(isset($_GET['next']))
	{
		$_SESSION['date'] = date("d-m-Y",strtotime("+1 month",strtotime($_SESSION['date'])));
	}
	else if(isset($_GET['prev']))
	{
		$_SESSION['date'] = date("d-m-Y",strtotime("-1 month",strtotime($_SESSION['date'])));
	}
	else if(isset($_GET['now']))
	{
		$_SESSION['date'] = date("d-m-Y",strtotime("now"));
	}

	

	 $day = date('d', strtotime($_SESSION['date']));      //Gets day of appointment (1‐31) 
	 $month = date('m', strtotime($_SESSION['date']));      //Gets month of appointment (1‐12) 
	 $year = date('Y', strtotime($_SESSION['date']));      //Gets year of appointment (e.g. 2016) 
	 $firstday = date('w', strtotime('01-' . $month . '-' . $year));  //Gets the day of the week for the 1st of  
							 //the month. (e.g. 0 for Sun, 1 for Mon) 
	 $days = date('t', strtotime($_SESSION['date']));      //Gets number of days in month 

	 $today = date('d');            //Gets today’s date 
	 $todaymonth = date('m');          //Gets today’s month 
	 $todayyear = date('Y');            //Gets today’s year
	 if(isset($_GET['title'])){
    $title = $_GET['title']; //get appointment title
  }  

   $today = date('d');            //Gets today’s date 
   $todaymonth = date('m');          //Gets today’s month 
   $todayyear = date('Y');            //Gets today’s year 

   if(isset($_GET['detail'])){
    $detail = $_GET['detail'];
  }
  else{
    $detail = '';
  }

  if(isset($_GET['$start_time'])){
    $start_time = $_GET['start_time'];    
  }
   
  if(isset($_GET['$end_time'])){
    $end_time = $_GET['end_time'];  
  }
  if(isset($_GET['$color'])){
    $color = $_GET['color'];
  }
?>

<?php
 $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "appointment";

if(isset($_GET['add_appo_btn']))
{
   $conn = new mysqli($servername,$username,$password,$dbname);
   $color = explode('#', $_GET['color']);
    $color = implode('', $color);
    if ($qry = "INSERT INTO `appo` (`id`, `username`, `title`, `date`, `start_time`, `end_time`, `detail`, `color`) 
    VALUES (NULL, '" .$_SESSION['user_id']. "', '".$title."', '".$_GET['date']."', '".$_GET['start_time']."', '".$_GET['end_time']."', '".$detail."', '".$color."')") {
      $color = explode('#', $_GET['color']);
      $color = implode('', $color);
      $conn->query($qry);
      echo $conn->error;
      $conn->close();

    header('Location: calendar_day.php?date='.$_GET['date']);
   }
}
?>
		
<?php
	$m1 = date('F', strtotime($_SESSION['date']));
	$t_day = date('l, F d', strtotime($_SESSION['date']));
    echo '<div class="month"><span id="monthIndi" value="' . $month . '">' . $m1 . '</span> <span id="yearIndi" value="' . $year . '">' . $year . '</span></div>';
?>
<a href="?now" title="<?php echo $t_day; ?>"><button class="todaybutton"> Today </button></a>
		<div class="left1">

			<?php 
					$d1 = date('D', strtotime($_SESSION['date']));
					$m1 = date('F', strtotime($_SESSION['date']));
					echo '<div class="top2">' ;
					//echo $d1 . ', ' . $m1 . ' ' . $year. '</br>' . 'Day ' . $day ;
					echo '<span id="monthIndi" value="' . $month . '">' . $m1 . '</span> <span id="yearIndi" value="' . $year . '">' . $year . '</span>';

					echo '</div>';
					

					

			?>

			<center>
				 <a href="?prev" title="Previous Month" class="prev2"> &lt; </a>
				 <a href="?next" title="Next Month" class="next2"> &gt; </a>
			</center>

			<div class="calendar3"> 
				 <div class="days days1">S</div> 
				 <div class="days days2">M</div> 
				 <div class="days days3">T</div> 
				 <div class="days days4">W</div> 
				 <div class="days days5">T</div> 
				 <div class="days days6">F</div> 
				 <div class="days days7">S</div> 

					<?php
						for($i=1; $i<=$firstday; $i++) 
						 { 
								echo '<div class="date blankday"></div>'; 
						 } 

							for($i=1; $i<=$days; $i++) 
						 { 
								echo '<div class="date date_h '; 
								if ($today == $i && $todaymonth==$month && $todayyear == $year) 
								{ 
									echo ' today'; 
								} 
									echo '"> <a href="?date=' . date("d-m-Y",strtotime(date($i."-m-Y",strtotime($_SESSION['date'])))) . '"><span class="dayIndi">' . $i . '</span></a> <br>'; 
					//echo '"><span class="dayIndi">' . $i . '</span><br>'; 

								
								echo  '</div>'; 
						 }

						$daysleft = 7-(($days + $firstday)%7); 
						 if($daysleft<7) 
						 { 
								for($i=1; $i<=$daysleft; $i++) 
								{ 
									 echo '<div class="date blankday"></div>'; 
								} 
						 } 

					?>
			</div>

			<div id="contact">+CREATE</div>
		      <div id="contactForm">
		      <form method="get">
		         <span id="title"><input type="text" class="form-control" name="title" id="title" placeholder="Add title" required></span><br>
		         <span id="position_d"><label for="date">&#128198;</label>
		         <input type="date" class="form-control" name="date" id="date" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 15px;"></span><br>
		         <span id="position_t"><label>&#9201;</label>
		         <label for="start_time">start </label>
		         <input type="time" class="form-control" name="start_time" id="start_time" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 17px;"></span><br>
		         <span id="position_t2">
		         <label for="end_time">end </label>
		         <input type="time" class="form-control" name="end_time" id="end_time" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 17px;"></span><br>
		         <span id="position_des"><label for="detail">&#128203;</label>
		         <input type="text" class="form-control" name="detail" id="detail" placeholder="Add description" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 15px;"></span><br>
		         <span id="position_c">
		         <label for="color" style="position: relative; top: -4px;">&#127912;</label>
		         <input type="color" class="form-control" name="color" id="color" required></span><br>
		         <input class="button3" name="add_appo_btn" type="submit" value="SAVE" />
		         <input class="button4" type="reset" value="DISMISS" />
		      </form>
		  </div>
		</div>

		<div class="dropdown">
           <button class="dropbtn">&#9662;View</button>
           <div class="dropdown-content" id="d" >
              <a href="calendar_day.php">DAY</a>
              <a href="calendar_week.php">WEEK</a>
              <a href="calendar.php">MONTH</a>
              <a href="detail.php">SCHEDULE</a>
          </div>
        </div>
  		</div>

  		<button class="logout"><a href="excode/includes/logout.php" style="text-decoration-line: none; color: white;">LOGOUT&#128682;</a></button>

		      <!-- 
			<div class="box_day2">
						<div class="dropdown">
							<button class="dropbtn">View</button>
							<div class="dropdown-content">
								<a href="day.php">Day</a>
								<a href="week.php"> Week </a>
								<a href="calendar.php">Month</a>
							</div>
						</div>
			</div>

			<div class="box_action2">
				<div class="dropdown">
					<button class="dropbtn">Action</button>
					<div class="dropdown-content">
						<a href="detail.php">detail</a>
						<a href="includes/logout.php">log out</a>
					</div>
				</div>
			</div>
 -->
			<!-- <div class="point_dot">
				 <?php
				 for ($i = 0; $i < 16; $i++) 
					{ 
						echo ' <span class="dot">' . '</span>';
					}
				?>
			</div> -->
		</div>

		<hr style=" border: 2px solid black; background-color: black; width: 99%; position: absolute; top: 84px; left: 5px;">
		<div class="vl"></div>
		<?php echo '<div class="day">' .$d1 . ', ' . $day. '</div>'; ?>

		<div class="right1" style="overflow:scroll;width: 920px;height: 530px;" id="style-2">
			 <div class="calendar2">  
							<?php
							$servername = "localhost";
						    $username = "root";
						    $password = "";
						    $dbname = "appointment";


							$con = mysqli_connect($servername,$username,$password,$dbname);
							$app_date = date($year.'-'.$month.'-'.$day);
							$sql = "SELECT * FROM `appo` WHERE `date` = '$app_date' AND username = '" . $_SESSION['user_id'] . "'" or die("Error:" . mysqli_error()); 
							$result = mysqli_query($con, $sql); 




								if(mysqli_num_rows($result) != 0)

								{

									while ($row = mysqli_fetch_array($result)) 
									{
										$row['start_time'] = date('H:i', strtotime($row['start_time']));
										$row['end_time'] = date('H:i', strtotime($row['end_time']));
										$row['detail'] =($row['detail']); 
										$row['color'] = ($row['color']);
										$events[] = $row;
									}
								}

							for ($i = 0; $i < 24; $i++) 
							{ 

								echo '<div class="droppable normal_date time2 date_p "> <span class="hour" style="float: left;">' .$i . '.00</span>' ;
								echo '<input type="hidden" id="month_val" value="' . $month. '">';
               					echo '<input type="hidden" id="year_val" value="' . $year . '">';
               					echo '<input type="hidden" id="day_val" value="' . $day . '">';
									
									if(isset($events))
									{
										foreach ($events as $anevent) 
										{
											if ($i == $anevent['start_time']) 
											{
												
												echo ' <span class=" draggable appointments   "  id="' . $anevent['id'] . '"> 
													   <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
												  	   <span style ="background-color: #'.$anevent['color'].'; "> '  
												  	   .$anevent['title'].'</span></div></span>';

												
											}
										}
									}	 
								echo '</div>';

							}


								
					?>
			</div>
			

		</div>




	


</body>
</html>