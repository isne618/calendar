<?php
/**
 * Copyright (C) 2013 peredur.net
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="regist_style.css">
    <head>
        <meta charset="UTF-8">
        <title>Register</title>
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
        <link rel="stylesheet" href="styles/main.css" />
    </head>
    <body style="background-color: #FFFACE;">
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        <h1 id="header">Registration</h1>
        <hr style="border: 5px dashed black; width: 500px; position: relative; bottom: 29px;">
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
        <h2 id="instruction" onclick="myFunction()">INSTRUCTIONS</h2>
        <p style="position: absolute; top: 143px; left: 245px; font-family: monospace;">*click text to view</p>
        <hr style="border: 3px solid black; position: relative; top: -45px; right: 620px; width: 200px;">
        <div id="toggle">
            <div class="box_inst"></div>
            <div class="group">
                <ul>
                    <li>Usernames may contain only digits, upper and lower case letters and underscores</li>
                    <li>Emails must have a valid email format</li>
                    <li>Passwords must be at least 6 characters long</li>
                    <li>Passwords must contain
                        <ul>
                            <li>At least one upper case letter (A..Z)</li>
                            <li>At least one lower case letter (a..z)</li>
                            <li>At least one number (0..9)</li>
                        </ul>
                    </li>
                    <li>Your password and confirmation must match exactly</li>
                </ul>
            </div>
        </div>
        <div id="align">
            <div class="box">
                <form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
                    <span id="f_name">Firstname: <input type='text' name='firstname' id='firstname' onfocus="f_name()" onblur="f_name2()">&nbsp;&nbsp;</span>
                    <span id="s_name">Lastname: <input type='text' name='lastname' id='lastname' onfocus="l_name()" onblur="l_name2()"/><br></span>
                    <span id="usr">Username: <input type='text' name='username' id='username' onfocus="usr()" onblur="usr2()"/><br></span>
                    <span id="mail">Email: <input type='text' name="email" id="email" onfocus="functions()" onblur="functions2()"/><br></span>
                    <span id="pwd">Password: <input type="password" name="password" id="password" onfocus="pwd()" onblur="pwd2()">&nbsp;&nbsp;</span>
                    <span id="conf">Confirm: <input type="password" name="confirmpwd" id="confirmpwd" onfocus="conf()" onblur="conf2()"/><br></span>
                    <input type="button" value="Register" class="button" onclick="return regformhash(this.form,this.form.firstname,this.form.lastname,
                    this.form.username,this.form.email,this.form.password,this.form.confirmpwd);" /> 
                </form>
                <hr style="border: 1px solid black; background-color: black;  width: 200px; position: relative; top: 90px;"> 
                <p style="font-family: monospace; font-size: 15px; position: relative; top: 90px;"><span style="color: white;">Return to the </span>
                <a href="index.php" class="effect-underline">log-in page</a>.</p>
            </div>
        </div>
        <script>
            function myFunction() {
                var x = document.getElementById("toggle");
                var y = document.getElementById("align");
                if (x.style.display === "block") {
                  x.style.display = "none";
                  y.style.display = "block";
                } else {
                  x.style.display = "block";
                  y.style.display = "none";
                }
            }

            document.getElementById("password") //can press enter to send the input
            .addEventListener("keyup", function(event) {
            event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("btn").click();
                }
            });

            function usr() {
                document.getElementById("username").style.borderColor = "#FF915B";
            }
            function usr2() {
                document.getElementById("username").style.borderColor = "black";
            }

            function f_name() {
                document.getElementById("firstname").style.borderColor = "#FF915B";
            }
            function f_name2() {
                document.getElementById("firstname").style.borderColor = "black";
            }

            function l_name() {
                document.getElementById("lastname").style.borderColor = "#FF915B";
            }
            function l_name2() {
                document.getElementById("lastname").style.borderColor = "black";
            }

            function functions() {
                document.getElementById("email").style.borderColor = "#FF915B";
            }
            function functions2() {
                document.getElementById("email").style.borderColor = "black";
            }

            function conf() {
                document.getElementById("confirmpwd").style.borderColor = "#FF915B";
            }
            function conf2() {
                document.getElementById("confirmpwd").style.borderColor = "black";
            }

            function pwd() {
                document.getElementById("password").style.borderColor = "#FF915B";
            }
            function pwd2() {
                document.getElementById("password").style.borderColor = "black";
            }
        </script>
    </body>
</html>

