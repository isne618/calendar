<?php
/**
 * Copyright (C) 2013 peredur.net
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Protected Page</title>
        <link rel="stylesheet" href="styles/main.css" />
    </head>
    <body>
        <?php if (login_check($mysqli) == true) : ?>
        <p>Welcome <?php echo htmlentities($_SESSION['username']); ?>!</p>
            <?php
    if(isset($_GET['date'])){
        $date = $_GET['date'];

        $day = date('d', strtotime($date));      //Gets day of apx`pointment (1‐31) 
        $month = date('m', strtotime($date));      //Gets month of appointment (1‐12) 
        $year = date('Y', strtotime($date));      //Gets year of appointment (e.g. 2016) 
        $firstday = date('w', strtotime('01-' . $month . '-' . $year));  //Gets the day of the week for the 1st of  
                 //the month. (e.g. 0 for Sun, 1 for Mon) 
        $days = date('t', strtotime($date));      //Gets number of days in month 
    }else{
        $date = date('Y-m-d');

        $day = date('d', strtotime($date));      //Gets day of apx`pointment (1‐31) 
        $month = date('m', strtotime($date));      //Gets month of appointment (1‐12) 
        $year = date('Y', strtotime($date));      //Gets year of appointment (e.g. 2016) 
        $firstday = date('w', strtotime('01-' . $month . '-' . $year));  //Gets the day of the week for the 1st of  
                 //the month. (e.g. 0 for Sun, 1 for Mon) 
        $days = date('t', strtotime($date));      //Gets number of days in month 
    }

   if(isset($_GET['title'])){
        $title = $_GET['title']; //get appointment title
    }  

   $today = date('d');            //Gets today’s date 
   $todaymonth = date('m');          //Gets today’s month 
   $todayyear = date('Y');            //Gets today’s year 

   if(isset($_GET['details'])){
        $details = $_GET['details'];
    }
    else{
        $details = '';
    }

    if(isset($_GET['$start_time'])){
        $start_time = $_GET['start_time'];      
    }
   
   if(isset($_GET['$end_time'])){
        $end_time = $_GET['end_time'];  
    }
?>

<div class="box_position">
<div class="box">
    <div class="position">
        <form action="../calendar.php" method="get">
            Date: <input type="date" name="date"><br />
            Title: <input type="text" placeholder="Enter title" name="title"><br />
            Time:<br /> 
            Start <input type="time" name="start_time"> 
            End <input type="time" name="end_time"><br />
            Detail: <input type="text" placeholder="Enter detail" name="detail"><br />
            <input type="submit" value="ADD">
        </form>
    </div>
</div>
</div>
            <p>If you are done, please <a href="includes/logout.php">log out</a></p> 
            <?php else : ?>
            <p>
                <span class="error">You are not authorized to access this page.</span> Please <a href="index.php">login</a>.
            </p>
        <?php endif; ?>
    </body>
</html>
