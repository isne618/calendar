<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="index_style.css">
    <head>
        <title>Log-in</title>
        <link rel="stylesheet" href="styles/main.css" />
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
    </head>
    <body style="background-color: #C3F5FF;">
        <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?>
        
        <h1 id="header">LOGIN</h1>
        <hr style="border: 5px dashed black; width: 500px; position: relative; bottom: 29px;">
        <div class="align_content">
            <div class="box2"></div> 
            <div class="box"></div>
            <div class="align">
                <form action="includes/process_login.php" method="post" name="login_form"> 			
                    <span id="email">Email: <input type="text" name="email" placeholder="Email address" id="mail" onfocus="functions()" onblur="functions2()"></span><br />
                    <span id="pwd">Password: <input type="password" name="password" id="password" placeholder="Password" onfocus="pwd()" onblur="pwd2()"></span><br />
                    <span id="show"><input type="checkbox" onclick="myFunction()">Show Password</span><br />
                    <span id="login_button"><input type="button" id="btn" value="Login" class="button" onclick="formhash(this.form, this.form.password);" /></span> 
                </form>
                <script>
                    function myFunction() {
                    var x = document.getElementById("password"); //show the password
                        if (x.type === "password") {
                            x.type = "text";
                        } else {
                            x.type = "password";
                        }
                    }
                    document.getElementById("password") //can press enter to send the input
                    .addEventListener("keyup", function(event) {
                    event.preventDefault();
                        if (event.keyCode === 13) {
                            document.getElementById("btn").click();
                        }
                    });

                    function functions() {
                        document.getElementById("mail").style.borderColor = "#8CECFF";
                    }
                    function functions2() {
                        document.getElementById("mail").style.borderColor = "black";
                    }

                    function pwd() {
                        document.getElementById("password").style.borderColor = "#8CECFF";
                    }
                    function pwd2() {
                        document.getElementById("password").style.borderColor = "black";
                    }
                </script>

                <hr style="border: 1px solid black; background-color: black;  width: 200px; position: relative; top: 50px;"> 
                <div class="position">
                    <p style="font-family: monospace; font-size: 15px;"><span style="color: #2274CD;">No account?? </span><a href="register.php" class="effect-underline">create one!!</a></p>
        
                    <p style="font-family: monospace; font-size: 12px;">You are currently logged <?php echo $logged ?>.</p>
                </div>
            </div>
        </div>
    </body>
</html>
