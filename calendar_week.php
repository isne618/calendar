<?php
  include_once 'excode/includes/db_connect.php';
  include_once 'excode/includes/functions.php';

sec_session_start();
if (!(login_check($mysqli) == true))
{
    header('Location: excode/index.php');
   exit();}

?>

<html>
    <title>Weekly calendar</title>
   <link rel="stylesheet" href="design_week.css" />
    <!-- <link rel="stylesheet" type="text/css" href="main.css"> -->

   <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js" type="text/javascript"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
      $(document).ready(function() {    
   
         $(".draggable").draggable({
              revert: "invalid",
              snap: true,
              snapMode: "inner"
            });
         $(".droppable").droppable({
            accept: ".draggable",
            drop: function(event, ui) 
            {
               var drop_id = ui.draggable.attr('id');
               var drop = $(this).children(".hour").text();
               var day = $(this).children("#day_val").val();
               var month = $(this).children("#month_val").val();
               var year = $(this).children("#year_val").val();
               var send_day = year + "-" + month + "-" + day;
               var send_time = drop + ":00:00";
               console.log(send_time);
               $.ajax({
                  url:"handlerweek.php",
                  method:"POST",
                  data:{start_time:send_time,date:send_day,event:drop_id},
                  success:function(data) {
                     if (!data) {
                        alert('fail');
                     }
                  }
               });
            }
         });

         
      });
   </script>
   <script>
      $(function() { 
        $('.normal_date').click(function() {
          $('#contactForm').fadeToggle();
          var day = $(this).find("#day_val").val();
          if(day.length == 1)
          {
            day = "0"+day;
          }
          $('#date').val($("#year_val").attr('value') + "-" + $("#month_val").attr('value') + "-" + day);
          console.log(day + "-" + $("#month_val").attr('value') + "-" + $("#year_val").attr('value') );
          
          // var time = $(this).children(".hour").text();
          // var send_time = time + ":00:00";



        });

        $('#contact').click(function() {
          $('#contactForm').fadeToggle();
          $('#date').val("");
        });


        $(document).mouseup(function (e) {
          var container = $("#contactForm");

          if (!container.is(e.target) 
              && container.has(e.target).length === 0) 
          {
              container.fadeOut();
          }
        });

        
      });
    </script>

   <!-- <script>
      $(function() {
        
        // contact form animations
        $('#contact').click(function() {
          $('#contactForm').fadeToggle();
        })
        $(document).mouseup(function (e) {
          var container = $("#contactForm");

          if (!container.is(e.target) // if the target of the click isn't the container...
              && container.has(e.target).length === 0) // ... nor a descendant of the container
          {
              container.fadeOut();
          }
        });
        
      });
  </script> -->
<body style="background-color: #FEFFF1">
   <?php if (login_check($mysqli) == true) : ?>
<?php 

if(isset($_GET['date']))
  {
    $_SESSION['date'] = date("d-m-Y",strtotime($_GET['date']));
  }
  else if(!isset($_SESSION['date']))
  {
    $_SESSION['date'] = date("d-m-Y");
  }

  if(isset($_GET['now']))
  {
    $_SESSION['date'] = date("d-m-Y",strtotime("now"));
  }

      $day = (isset($_SESSION['week'])) ? $_SESSION['week'] : date('d');
      $weekgetdate = $day = date('W', strtotime($_SESSION['date']));
      $week = (isset($_SESSION['week'])) ? $_SESSION['week'] : date('W', strtotime($_SESSION['date']));
      $month = date('F', strtotime($_SESSION['date']));
      $month_num = date('m', strtotime($_SESSION['date']));
      $year = (isset($_SESSION['year'])) ? $_SESSION['year'] : date("Y");
      $title = "";
      $today = date('d');
      $todayweek = date('W');
      $todaymonth = date('m');
      $todayyear = date('Y');

    if($week > 52){
      $year++;
      $week = 1;
    }
    else if($week < 1){
      $year--;
      $week = 52;
    }
 if(isset($_GET['title'])){
    $title = $_GET['title']; //get appointment title
  }  

   $today = date('d');            //Gets today’s date 
   $todaymonth = date('m');          //Gets today’s month 
   $todayyear = date('Y');            //Gets today’s year 

   if(isset($_GET['detail'])){
    $detail = $_GET['detail'];
  }
  else{
    $detail = '';
  }

  if(isset($_GET['$start_time'])){
    $start_time = $_GET['start_time'];    
  }
   
  if(isset($_GET['$end_time'])){
    $end_time = $_GET['end_time'];  
  }
  if(isset($_GET['$color'])){
    $color = $_GET['color'];
  }

$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "appointment";

if(isset($_GET['add_appo_btn']))
{
   $conn = new mysqli($servername,$username,$password,$dbname);
   $color = explode('#', $_GET['color']);
    $color = implode('', $color);
    if ($qry = "INSERT INTO `appo` (`id`, `username`, `title`, `date`, `start_time`, `end_time`, `detail`, `color`) 
    VALUES (NULL, '" .$_SESSION['user_id']. "', '".$title."', '".$_GET['date']."', '".$_GET['start_time']."', '".$_GET['end_time']."', '".$detail."', '".$color."')") {
      $color = explode('#', $_GET['color']);
      $color = implode('', $color);
      $conn->query($qry);
      echo $conn->error;
      $conn->close();

    header('Location: calendar_week.php?date='.$_GET['date']);
   }
}
?>
           <div style="position: relative; top: 10px;">
          <!-- show day -->
          <?php 
          
          $m1 = date('F', strtotime($day));
          echo '<div class="month"><span id="monthIndi" value="' . $month . '">' . $month . '</span> <span id="yearIndi" value="' . $year . '">' . $year . '</span>';

          //echo $month . ' ' . $year;
          echo '</div>';
          $username = ($_SESSION['username']);
          $t_day = date('l, F d', strtotime($_SESSION['date']));

          ?> 


      <?php
      echo '<a title="Previous Week" class="button" href="' . $_SERVER['PHP_SELF'] . '?week=' . ($week == 1 ? 52 : $week -1) . '&year=' . ($week == 1 ? $year - 1 : $year) . '&date=' . date("Y-m-d", strtotime("-1 week", strtotime($_SESSION['date']))) . '"> &#8810; Prev </center></a>';
      echo '<a title="Next Week" class="button2" href="' . $_SERVER['PHP_SELF'] . '?week=' . ($week == 52 ? 1 : 1 + $week) . '&year=' . ($week == 52 ? 1 + $year : $year) . '&date=' . date("Y-m-d", strtotime("+1 week", strtotime($_SESSION['date']))) . '"> Next &#8811; </a>'; 
      echo '<a href="?date=' . date('Y-m-d', strtotime('now')) . '" title="'.$t_day.'"><button class="todaybutton"> Today </button></a>';     
      ?> 
      <button class="logout"><a href="excode/includes/logout.php" style="text-decoration-line: none; color: white;">LOGOUT&#128682;</a></button>
      <hr style=" border: 2px solid black; background-color: black; width: 99%; position: absolute; top: 84px; left: 8px;">
      <div class="vl"></div>
      <div id="contact">+CREATE</div>
      <div id="contactForm">
      <form method="get">
         <span id="title"><input type="text" class="form-control" name="title" id="title" placeholder="Add title" required></span><br>
         <span id="position_d"><label for="date">&#128198;</label>
         <input type="date" class="form-control" name="date" id="date" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 15px;"></span><br>
         <span id="position_t"><label>&#9201;</label>
         <label for="start_time">start </label>
         <input type="time" class="form-control" name="start_time" id="start_time" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 17px;"></span><br>
         <span id="position_t2">
         <label for="end_time">end </label>
         <input type="time" class="form-control" name="end_time" id="end_time" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 17px;"></span><br>
         <span id="position_des"><label for="detail">&#128203;</label>
         <input type="text" class="form-control" name="detail" id="detail" placeholder="Add description" required style="background-color: #FBFBFB; padding: 3px; border: 0px; font-family: monospace; font-size: 15px;"></span><br>
         <span id="position_c">
         <label for="color" style="position: relative; top: -4px;">&#127912;</label>
         <input type="color" class="form-control" name="color" id="color" required></span><br>
         <input class="button3" name="add_appo_btn" type="submit" value="SAVE" />
         <input class="button4" type="reset" value="DISMISS" />
      </form>
   </div>

       <div class="dropdown">
           <button class="dropbtn">&#9662;View</button>
           <div class="dropdown-content" id="d" >
              <a href="calendar_day.php">DAY</a>
              <a href="calendar_week.php">WEEK</a>
              <a href="calendar.php">MONTH</a>
              <a href="detail.php">SCHEDULE</a>
          </div>
        </div>

  </div>

  <!-- <div class="right">  
      <div class="right_top">
            <?php
            echo 'Name: '.$username;
            ?>
      </div>
  </div>  -->



<?php
      if ($week < 10) {
         $week = '0'. $week;
      }
      echo '<div class="calendar" style="overflow:scroll; width:1019px;height:569px;" id="style-2">';
      for ($day = 0; $day <= 6; $day++) {
         $d = strtotime($year ."W". $week . $day);
         echo '<div style="float: left;">';

         if (date('l', $d) == 'Sunday') {

             $startdate = date('Y-m-d', $d);
        $connect = mysqli_connect("localhost", "root", "", "appointment");
        $sql = "SELECT * FROM `appo` WHERE `date` = '$startdate' AND `username` = '". $_SESSION['user_id'] ."'";
        $result = mysqli_query($connect, $sql);
            echo '<h3><div class=" days sun"><b>' . date('D', $d) . ', ' . date('d M', $d) . '</b></div></h3>';
            for ($i = 0; $i < 24; $i++) {
               echo '<div class="droppable normal_date timeweek "><span class="hour  ">' . $i . "</span>.00<br>";
               echo '<input type="hidden" id="month_val" value="' . $month_num . '">';
               echo '<input type="hidden" id="year_val" value="' . $year . '">';
               echo '<input type="hidden" id="day_val" value="' . date('d', $d) . '">';
               if (mysqli_num_rows($result) != 0) {
                  while($row = mysqli_fetch_array($result)) {
                     $events_sun[] = $row;
                  }
                  foreach ($events_sun as $anevent) {
                    if ($i == $anevent['start_time']) {
                        echo ' <span class=" draggable    "  id="' . $anevent['id'] . '"> 
                               <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
                               <span style ="background-color: #'.$anevent['color'].'; "> '  
                               .$anevent['title'].'</span></div></span>';
                     }
                  }

                 
               }
               echo '</div>';
            }
         }
         
         if (date('l', $d) == 'Monday') {
             $startdate = date('Y-m-d', $d);
        $connect = mysqli_connect("localhost", "root", "", "appointment");
        $sql = "SELECT * FROM `appo` WHERE `date` = '$startdate' AND `username` = '". $_SESSION['user_id'] ."'";
        $result = mysqli_query($connect, $sql);
            echo '<h3><div class=" days mon" ><b>' . date('D', $d) . ', ' . date('d M', $d) . '</b></div></h3>';
            for ($i = 0; $i < 24; $i++) { 
               echo '<div class="droppable normal_date timeweek"><span class="hour  ">' . $i . "</span>.00<br>";
               echo '<input type="hidden" id="month_val" value="' . $month_num . '">';
               echo '<input type="hidden" id="year_val" value="' . $year . '">';
               echo '<input type="hidden" id="day_val" value="' . date('d', $d) . '">';
               if (mysqli_num_rows($result) != 0) {
                  while ($row = mysqli_fetch_array($result)) {
                     $events_mon[] = $row;
                  }
                  foreach ($events_mon as $anevent) {
                    if ($i == $anevent['start_time']) {
                        echo ' <span class=" draggable    "  id="' . $anevent['id'] . '"> 
                               <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
                               <span style ="background-color: #'.$anevent['color'].'; "> '  
                               .$anevent['title'].'</span></div></span>';
                     }
                  }

                  
               }
               echo '</div>';
            }
         }

         if (date('l', $d) == 'Tuesday') {
             $startdate = date('Y-m-d', $d);
        $connect = mysqli_connect("localhost", "root", "", "appointment");
        $sql = "SELECT * FROM `appo` WHERE `date` = '$startdate' AND `username` = '". $_SESSION['user_id'] ."'";
        $result = mysqli_query($connect, $sql);
            echo '<h3><div class="days tue"><b>' . date('D', $d) . ', ' . date('d M', $d) . '</b></div></h3>';
            for ($i = 0; $i < 24; $i++) {
               echo '<div class="droppable normal_date timeweek"><span class="hour " s>' . $i . "</span>.00<br>";
               echo '<input type="hidden" id="month_val" value="' . $month_num . '">';
               echo '<input type="hidden" id="year_val" value="' . $year . '">';
               echo '<input type="hidden" id="day_val" value="' . date('d', $d) . '">';
               if (mysqli_num_rows($result) != 0) {
                  while($row = mysqli_fetch_array($result)) {
                     $events_tue[] = $row;
                  }
                  foreach ($events_tue as $anevent) {
                    if ($i == $anevent['start_time']) {
                        echo ' <span class=" draggable    "  id="' . $anevent['id'] . '"> 
                               <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
                               <span style ="background-color: #'.$anevent['color'].'; "> '  
                               .$anevent['title'].'</span></div></span>';
                     }
                  }

                  
               }
               echo '</div>';
            }
         }

         if (date('l', $d) == 'Wednesday') {
             $startdate = date('Y-m-d', $d);
        $connect = mysqli_connect("localhost", "root", "", "appointment");
        $sql = "SELECT * FROM `appo` WHERE `date` = '$startdate' AND `username` = '". $_SESSION['user_id'] ."'";
        $result = mysqli_query($connect, $sql);
            echo '<h3><div class="days wed"><b>' . date('D', $d) . ', ' . date('d M', $d) . '</b></div></h3>';
            for ($i = 0; $i < 24; $i++) {
               echo '<div class="droppable normal_date timeweek"><span class="hour ">' . $i . "</span>.00<br>";
               echo '<input type="hidden" id="month_val" value="' . $month_num . '">';
               echo '<input type="hidden" id="year_val" value="' . $year . '">';
               echo '<input type="hidden" id="day_val" value="' . date('d', $d) . '">';
               if (mysqli_num_rows($result) != 0) {
                  while($row = mysqli_fetch_array($result)) {
                     $events_wed[] = $row;
                  }
                  foreach ($events_wed as $anevent) {
                    if ($i == $anevent['start_time']) {
                        echo ' <span class=" draggable    "  id="' . $anevent['id'] . '"> 
                               <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
                               <span style ="background-color: #'.$anevent['color'].'; "> '  
                               .$anevent['title'].'</span></div></span>';
                     }
                  }

                  
               }
               echo '</div>';
            }
         }

         if (date('l', $d) == 'Thursday') {
            $startdate = date('Y-m-d', $d);
        $connect = mysqli_connect("localhost", "root", "", "appointment");
        $sql = "SELECT * FROM `appo` WHERE `date` = '$startdate' AND `username` = '". $_SESSION['user_id'] ."'";
        $result = mysqli_query($connect, $sql);
            echo '<h3><div class="days thr" ><b>' . date('D', $d) . ', ' . date('d M', $d) . '</b></div></h3>';
            for ($i = 0; $i < 24; $i++) {
               echo '<div class="droppable normal_date timeweek"><span class="hour  ">' . $i . "</span>.00<br>";
               echo '<input type="hidden" id="month_val" value="' . $month_num . '">';
               echo '<input type="hidden" id="year_val" value="' . $year . '">';
               echo '<input type="hidden" id="day_val" value="' . date('d', $d) . '">';


               if (mysqli_num_rows($result) != 0) {
                  while($row = mysqli_fetch_array($result)) {
                     $events_thu[] = $row;
                  }
                  foreach ($events_thu as $anevent) {
                    if ($i == $anevent['start_time']) {
                        echo ' <span class=" draggable    "  id="' . $anevent['id'] . '"> 
                               <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
                               <span style ="background-color: #'.$anevent['color'].'; "> '  
                               .$anevent['title'].'</span></div></span>';
                     }
                  }


        
               }
               echo '</div>';
            }
         }

         if (date('l', $d) == 'Friday') {
             $startdate = date('Y-m-d', $d);
        $connect = mysqli_connect("localhost", "root", "", "appointment");
        $sql = "SELECT * FROM `appo` WHERE `date` = '$startdate' AND `username` = '". $_SESSION['user_id'] ."'";
        $result = mysqli_query($connect, $sql);
            echo '<h3><div class="days fri" ><b>' . date('D', $d) . ', ' . date('d M', $d) . '</b></div></h3>';
            for ($i = 0; $i < 24; $i++) {
               echo '<div class="droppable normal_date timeweek"><span class="hour  ">' . $i . "</span>.00<br>";
               echo '<input type="hidden" id="month_val" value="' . $month_num . '">';
               echo '<input type="hidden" id="year_val" value="' . $year . '">';
               echo '<input type="hidden" id="day_val" value="' . date('d', $d) . '">';
               if (mysqli_num_rows($result) != 0) {
                  while($row = mysqli_fetch_array($result)) {
                     $events_fri[] = $row;
                  }
                 foreach ($events_fri as $anevent) {
                    if ($i == $anevent['start_time']) {
                        echo ' <span class=" draggable    "  id="' . $anevent['id'] . '"> 
                               <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
                               <span style ="background-color: #'.$anevent['color'].'; "> '  
                               .$anevent['title'].'</span></div></span>';
                     }
                  }

                  
               }
               echo '</div>';
            }
         }

         if (date('l', $d) == 'Saturday') {
            $startdate = date('Y-m-d', $d);
        $connect = mysqli_connect("localhost", "root", "", "appointment");
        $sql = "SELECT * FROM `appo` WHERE `date` = '$startdate' AND `username` = '". $_SESSION['user_id'] ."'";
        $result = mysqli_query($connect, $sql);
            echo '<h3><div class="days sat" ><b>' . date('D', $d) . ', ' . date('d M', $d) . '</b></div></h3>';
            for ($i = 0; $i < 24; $i++) {
               echo '<div class="droppable normal_date timeweek"><span class="hour  ">' . $i . "</span>.00<br>";
               echo '<input type="hidden" id="month_val" value="' . $month_num . '">';
               echo '<input type="hidden" id="year_val" value="' . $year . '">';
               echo '<input type="hidden" id="day_val" value="' . date('d', $d) . '">';
               if (mysqli_num_rows($result) != 0) {
                  while($row = mysqli_fetch_array($result)) {
                     $events_sat[] = $row;
                  }
                  foreach ($events_sat as $anevent) {
                    if ($i == $anevent['start_time']) {
                        echo ' <span class=" draggable    "  id="' . $anevent['id'] . '"> 
                               <div title = "start: '.$anevent['start_time'].'&#xA;end: '.$anevent['end_time'].' &#xA;detail: '.$anevent['detail'].'   " >  
                               <span style ="background-color: #'.$anevent['color'].'; "> '  
                               .$anevent['title'].'</span></div></span>';
                     }
                  }

              
               }
               echo '</div>';
            }
         }

         
         echo '</div>';
      }
      echo '</div>';
      echo '</div>';
   ?>
   </div>
   <?php else : ?>
      <h2>Please <a href="index.php">Log in</a></h2>
   <?php endif; ?>
</body>
</html>