<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="detail_css.css">
<head>
	<title>Schedule</title>
	<meta charset="UTF-8">
	<script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
   	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js" type="text/javascript"></script>
   	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
	include_once 'excode/includes/db_connect.php';
	include_once 'excode/includes/functions.php';

	sec_session_start();
	if (!(login_check($mysqli) == true))
	{
	   header('Location: excode/index.php');
	   exit();
	}
?>
</head>
<body style="background-color: #FEFFF1;">
<?php
	ini_set('display_errors', 1);
	error_reporting(~0);

	$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "appointment";

	$conn = mysqli_connect($servername,$username,$password,$dbname);

	$sql = "SELECT * FROM appo";

	$query = mysqli_query($conn,$sql);

	// $previous = "javascript:history.go(-1)";
	// if(isset($_SERVER['HTTP_REFERER'])) {
 //    $previous = $_SERVER['HTTP_REFERER'];
	// }
?>
<div style="position: relative; right: 229px;">
<div class="vl"></div>
<div class="dropdown">
   <button class="dropbtn">&#9666;View</button>
   <div class="dropdown-content" id="d" >
      <a href="calendar_day.php">DAY</a>
      <a href="calendar_week.php">WEEK</a>
      <a href="calendar.php">MONTH</a>
      <a href="detail.php">SCHEDULE</a>
  </div>
</div>
<button class="logout"><a href="excode/includes/logout.php" style="text-decoration-line: none; color: white;">LOGOUT&#128682;</a></button>
</div>

<div style="position: relative;top: 6px; left: -76px;">
<h1 style="h1">SCHEDULE</h1>
<hr style=" border: 3px solid black; background-color: black; width: 32%; position: absolute; right: 523px; top: 140px;">


<div style="position: relative; top:35px; right: 118px;">
<div style="border-radius: 15px; height: 35px; width: 82px; background-color: #F3D56F; position: absolute; left: 411px; top: 187px;"></div>
<h2 style="position: absolute; left: 417px;">TITLE</h2>
<div style="border-radius: 15px; height: 35px; width: 82px; background-color: #F3D56F; position: absolute; left: 631px; top: 187px;"></div>
<h2 style="position: absolute; left: 639px;">Y-M-D</h2>
<div style="border-radius: 15px; height: 35px; width: 82px; background-color: #F3D56F; position: absolute; right: 582px; top: 187px;"></div>
<h2 style="position: absolute; right: 594px;">TIME</h2>
<div style="border-radius: 15px; height: 35px; width: 169px; background-color: #F3D56F; position: absolute; right: 310px; top: 187px;"></div>
<h2 style="position: absolute; right: 318px;">DESCRIPTION</h2>
<table>
<?php
while($result=mysqli_fetch_array($query,MYSQLI_ASSOC))
{
?>
  <tr>
  	<?php echo '<td style =" background-color: #'.$result["color"].' ;"></td> ';?>
    <!-- <input type="hidden"><?php echo $result["id"];?> -->
    <td style="width: 200px;"><?php echo $result["title"];?></td>
    <td style="width: 180px;"><?php echo $result["date"];?></td>
    <td style="width: 100px;"><?php echo $result["start_time"]; echo "-"?></td>
    <td style="width: 120px;"><?php echo $result["end_time"];?></td>
    <td style="width: 200px;"><?php echo $result["detail"];?></td>
    <td style="width: 50px;"><a href="edit.php?id=<?php echo $result["id"];?>">&#128221;</a></td>
    <td style="width: 100px;"><a href="JavaScript:if(confirm('Confirm Delete?')==true){window.location='delete.php?id=<?php echo $result["id"];?>';}">&#9940;</a></td>
  </tr>
  </div>
<?php
}
?>
</table>
<?php
mysqli_close($conn);
?>
</div>
</body>
</html>